package com.gildedrose;

class GildedRose {
    public static final String DEXTERITY_VEST = "+5 Dexterity Vest";
    public static final String ELIXIR_OF_THE_MONGOOSE = "Elixir of the Mongoose";
    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES_TO_A_TAFKAL_80_ETC_CONCERT = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS_HAND_OF_RAGNAROS = "Sulfuras, Hand of Ragnaros";
    public static final String CONJURED = "Conjured Mana Cake";
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        if (items != null) {
            for (Item item : items) {
                setNewItemValues(item);
            }
        }
    }

    private void setNewItemValues(Item item) {
        if (!item.name.equals(SULFURAS_HAND_OF_RAGNAROS)) {
            decrementSellIn(item);
        }
        if (item.name.equals(DEXTERITY_VEST) || item.name.equals(ELIXIR_OF_THE_MONGOOSE)) {
            decrementQuality(item);
        }
        if (item.name.equals(AGED_BRIE) || item.name.equals(BACKSTAGE_PASSES_TO_A_TAFKAL_80_ETC_CONCERT)) {
            incrementQuality(item);
        }
        if (item.name.equals(CONJURED)) {
            decrementQuality(item);
            decrementQuality(item);
        }
    }

    private void decrementQuality(Item item) {
        if (item.sellIn <= 0) {
            looseQuality(item);
        } else {
            --item.quality;
        }
        setLowEdgeQuality(item);
    }

    private void incrementQualityAfterSellInLess(Item item) {
        if (item.sellIn <= 10 && item.sellIn > 5) {
            item.quality += 2;
        } else if (item.sellIn <= 5) {
            item.quality += 3;
        } else {
            ++item.quality;
        }
    }

    private void looseQuality(Item item) {
        item.quality -= 2;
    }

    private void setLowEdgeQuality(Item item) {
        if (item.quality <= 0) {
            item.quality = 0;
        }
    }

    private void setLowEdgeSellIn(Item item) {
        if (item.sellIn <= 0) {
            item.sellIn = 0;
        }
    }

    private void setHighEdgeQuality(Item item) {
        if (item.quality >= 50) {
            item.quality = 50;
        }
    }

    private void incrementQuality(Item item) {
        incrementQualityAfterSellInLess(item);
        setHighEdgeQuality(item);
    }

    private void decrementSellIn(Item item) {
        --item.sellIn;
        setLowEdgeSellIn(item);
    }
}