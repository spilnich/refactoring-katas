package com.gildedrose;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    private GildedRose gildedRose;

    @BeforeEach
    void setUp() {
        Item[] items = new Item[]{
                new Item("+5 Dexterity Vest", 10, 20), //
                new Item("Aged Brie", 2, 0), //
                new Item("Elixir of the Mongoose", 5, 7), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                new Item("Conjured Mana Cake", 3, 6)};
        gildedRose = new GildedRose(items);
    }

    @Test
    void foo() {
        Item[] items = new Item[] { new Item("foo", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
    }

    @Test
    @DisplayName("Should return correct quality")
    void updateQuality() {
        for (int i = 0; i < 3; i++) {
            gildedRose.updateQuality();
        }
        assertEquals(17, gildedRose.items[0].quality);
        assertEquals(9, gildedRose.items[1].quality);
        assertEquals(4, gildedRose.items[2].quality);
        assertEquals(80, gildedRose.items[3].quality);
        assertEquals(80, gildedRose.items[4].quality);
        assertEquals(23, gildedRose.items[5].quality);
        assertEquals(50, gildedRose.items[6].quality);
        assertEquals(50, gildedRose.items[7].quality);
        assertEquals(0, gildedRose.items[8].quality);
    }

    @Test
    @DisplayName("Should return correct quality")
    void updateQualityMoreIteration() {
        for (int i = 0; i < 10; i++) {
            gildedRose.updateQuality();
        }
        assertEquals(9, gildedRose.items[0].quality);
        assertEquals(30, gildedRose.items[1].quality);
        assertEquals(0, gildedRose.items[2].quality);
        assertEquals(80, gildedRose.items[3].quality);
        assertEquals(80, gildedRose.items[4].quality);
        assertEquals(37, gildedRose.items[5].quality);
        assertEquals(50, gildedRose.items[6].quality);
        assertEquals(50, gildedRose.items[7].quality);
        assertEquals(0, gildedRose.items[8].quality);
    }
}
