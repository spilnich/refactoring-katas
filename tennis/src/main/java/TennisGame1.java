
public class TennisGame1 implements TennisGame {

    private final String player1;
    private final String player2;
    private int firstPlayerScore;
    private int secondPlayerScore;

    public TennisGame1() {
        player1 = "player1";
        player2 = "player2";
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(player1)) {
            firstPlayerScore += 1;
        } else if (playerName.equals(player2)) {
            secondPlayerScore += 1;
        }
    }

    public String getScore() {
        String score = "";
        if (isScoresEqual(firstPlayerScore, secondPlayerScore)) {
            return getScoreByFirstPlayerScore();
        } else if (isScoresGreaterFour()) {
            return getScoreByMinusResult();
        } else {
            return getScoreBySecondPlayerScore(score);
        }
    }

    private String getScoreBySecondPlayerScore(String score) {
        int tempScore;
        for (int i = 1; i < 3; i++) {
            if (isScoresEqual(i, 1)) {
                tempScore = firstPlayerScore;
            } else {
                score = score.concat("-");
                tempScore = secondPlayerScore;
            }
            score = getScoreByPlayerScore(score, tempScore);
        }
        return score;
    }

    private String getScoreByPlayerScore(String score, int tempScore) {
        switch (tempScore) {
            case 0:
                score += ScoreType.LOVE.label;
                break;
            case 1:
                score += ScoreType.FIFTEEN.label;
                break;
            case 2:
                score += ScoreType.THIRTY.label;
                break;
            case 3:
                score += ScoreType.FORTY.label;
                break;
            default:
                break;
        }
        return score;
    }

    private String getScoreByMinusResult() {
        String score;
        if (isScoresEqual(getMinusResult(), 1)) {
            score = "Advantage " + player1;
        } else if (isScoresEqual(getMinusResult(), -1)) {
            score = "Advantage " + player2;
        } else if (getMinusResult() >= 2) {
            score = "Win for " + player1;
        } else {
            score = "Win for " + player2;
        }
        return score;
    }

    private boolean isScoresGreaterFour() {
        return firstPlayerScore >= 4 || secondPlayerScore >= 4;
    }

    private int getMinusResult() {
        return firstPlayerScore - secondPlayerScore;
    }

    private String getScoreByFirstPlayerScore() {
        switch (firstPlayerScore) {
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";
            case 2:
                return "Thirty-All";
            default:
                return "Deuce";
        }
    }

    private boolean isScoresEqual(int firstScore, int secondScore) {
        return firstScore == secondScore;
    }
}
