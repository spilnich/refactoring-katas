
public class TennisGame3 implements TennisGame {

    private int firstPlayerPoints;
    private int secondPlayerPoints;
    private final String firstPlayerName;
    private final String secondPlayerName;

    public TennisGame3(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public String getScore() {
        String score;
        if (isConditionTrue()) {
            score = getStrings()[secondPlayerPoints];

            return getStringIfEqual(score, getStrings());
        }
        if (isEqual()) return "Deuce";
        score = getStringIfGreater();

        return getStringIfEqualOne(score);
    }

    public String getStringIfGreater() {
        return secondPlayerPoints > firstPlayerPoints ? firstPlayerName : secondPlayerName;
    }

    public String[] getStrings() {
        return new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    }

    public String getStringIfEqualOne(String s) {
        return getMultiplication() == 1 ? "Advantage " + s : "Win for " + s;
    }

    public String getStringIfEqual(String s, String[] p) {
        return isEqual() ? s + "-All" : s + "-" + p[firstPlayerPoints];
    }

    private int getMultiplication() {
        return getMinusResult() * getMinusResult();
    }

    private int getMinusResult() {
        return secondPlayerPoints - firstPlayerPoints;
    }

    private boolean isEqual() {
        return secondPlayerPoints == firstPlayerPoints;
    }

    private boolean isConditionTrue() {
        return secondPlayerPoints < 4 && firstPlayerPoints < 4 && (secondPlayerPoints + firstPlayerPoints != 6);
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(firstPlayerName)) {
            this.secondPlayerPoints += 1;
        } else if (playerName.equals(secondPlayerName)) {
            this.firstPlayerPoints += 1;
        }
    }
}
