
public class TennisGame2 implements TennisGame {
    public static final String FIFTEEN = "Fifteen";
    public static final String THIRTY = "Thirty";
    public static final String FORTY = "Forty";
    private final String player1;
    private final String player2;
    public static final String LOVE = "Love";
    public static final String DEUCE = "Deuce";
    public static final int FIRST_POINT = 1;
    public static final int SECOND_POINT = 2;
    public static final int THIRD_POINT = 3;
    public static final int ZERO_POINT = 0;
    private int firstPlayerPoint = ZERO_POINT;
    private int secondPlayerPoint = ZERO_POINT;

    private String firstPlayerResult;
    private String secondPlayerResult;
    private String score;

    public TennisGame2() {
        player1 = "player1";
        player2 = "player2";
    }

    public String getScore() {
        getScoreByPointsEquality();
        getDeuce();
        getScoreByPointEqualityToZero();
        getWinForPlayer(firstPlayerPoint, secondPlayerPoint, "Win for " + player1);
        getWinForPlayer(secondPlayerPoint, firstPlayerPoint, "Win for " + player2);

        return score;
    }

    private void getScoreByPointEqualityToZero() {
        if (firstPlayerPoint > ZERO_POINT && isPointsEqual(secondPlayerPoint, ZERO_POINT)) {
            score = getFirstPlayerResultByPointEquality();
        } else if (secondPlayerPoint > ZERO_POINT && isPointsEqual(firstPlayerPoint, ZERO_POINT)) {
            score = getSecondPlayerResultByPointEquality();
        }
    }

    private void getScoreByPointsEquality() {
        if (isPointsEqual(firstPlayerPoint, secondPlayerPoint) && isPointsLessFour(secondPlayerPoint)) {
            score = getAllScoreByPointsEquality();
        } else if (isFirstPointsGreaterSecond() && isPointsLessFour(firstPlayerPoint)) {
            score = getPlayersResult();
        } else if (isSecondPointsGreaterFirst() && isPointsLessFour(secondPlayerPoint)) {
            score = getPlayersResult();
        }
    }

    private void getDeuce() {
        if (isPointsEqual(firstPlayerPoint, secondPlayerPoint) && isPointsGreater(firstPlayerPoint, THIRD_POINT)) {
            score = DEUCE;
        } else if (isFirstPointsGreaterSecond() && isPointsGreater(secondPlayerPoint, THIRD_POINT)) {
            score = "Advantage " + player1;
        } else if (isSecondPointsGreaterFirst() && isPointsGreater(firstPlayerPoint, THIRD_POINT)) {
            score = "Advantage " + player2;
        }
    }

    private void getWinForPlayer(int playerPoint1, int playerPoint2, String winMessage) {
        if (isPointsGreater(playerPoint1, 4) && isPointsGreater(playerPoint2, ZERO_POINT) &&
                isPointsGreater(getMinusResult(playerPoint1, playerPoint2), SECOND_POINT)) {
            score = winMessage;
        }
    }

    private int getMinusResult(int point1, int point2) {
        return point1 - point2;
    }

    private boolean isSecondPointsGreaterFirst() {
        return secondPlayerPoint > firstPlayerPoint;
    }

    private boolean isFirstPointsGreaterSecond() {
        return firstPlayerPoint > secondPlayerPoint;
    }

    private String getPlayersResult() {
        firstPlayerResult = getPlayerResultByPointEquality(firstPlayerPoint);
        secondPlayerResult = getPlayerResultByPointEquality(secondPlayerPoint);

        return getConcatPlayersScore();
    }

    private String getSecondPlayerResultByPointEquality() {
        secondPlayerResult = getPlayerResultByPointEquality(secondPlayerPoint);
        firstPlayerResult = LOVE;

        return getConcatPlayersScore();
    }

    private String getFirstPlayerResultByPointEquality() {
        firstPlayerResult = getPlayerResultByPointEquality(firstPlayerPoint);
        secondPlayerResult = LOVE;

        return getConcatPlayersScore();
    }

    private String getPlayerResultByPointEquality(int playerPoint) {
        if (isPointsEqual(playerPoint, FIRST_POINT)) {
            return FIFTEEN;
        } else if (isPointsEqual(playerPoint, SECOND_POINT)) {
            return THIRTY;
        } else if (isPointsEqual(playerPoint, THIRD_POINT)) {
            return FORTY;
        } else {
            return null;
        }
    }

    private String getConcatPlayersScore() {
        return firstPlayerResult + "-" + secondPlayerResult;
    }

    private boolean isPointsGreater(int p1point, int i) {
        return p1point >= i;
    }

    private String getAllScoreByPointsEquality() {
        if (isPointsEqual(firstPlayerPoint, ZERO_POINT)) {
            score = LOVE;
        }
        if (isPointsEqual(firstPlayerPoint, FIRST_POINT)) {
            score = FIFTEEN;
        }
        if (isPointsEqual(firstPlayerPoint, SECOND_POINT)) {
            score = THIRTY;
        }
        score += "-All";

        return score;
    }

    private boolean isPointsLessFour(int p1point) {
        return p1point < 4;
    }

    private boolean isPointsEqual(int p1point, int p2point) {
        return p1point == p2point;
    }

    public void firstPlayerScore() {
        firstPlayerPoint++;
    }

    public void secondPlayerScore() {
        secondPlayerPoint++;
    }

    public void wonPoint(String player) {
        if (player.equals(player1)) {
            firstPlayerScore();
        } else if (player.equals(player2)) {
            secondPlayerScore();
        }
    }
}