public enum ScoreType {

    LOVE("Love"),
    FIFTEEN("Fifteen"),
    THIRTY("Thirty"),
    FORTY("Forty");

    public final String label;

    ScoreType(String label) {
        this.label = label;
    }
}
