public class NumberToName {

    private static final String HUNDRED = "hundred";
    private static final String THOUSAND = "thousand";
    private final String[] oneDigitNames = new String[]{
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    private final String[] tensNames = new String[]{
            "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private final String[] tenToTwentyNames = new String[]{
            "", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private String result;

    public String convertNumber(int number) {

        getOneDigitNumber(number);
        getNumberFromTenToTwenty(number);
        getTwoDigitNumber(number);
        getThreeDigitNumber(number);
        getFourDigitNumber(number);

        return result;
    }

    private void getOneDigitNumber(int number) {
        if (number < 10 && number >= 0) {
            result = oneDigitNames[number];
        }
    }

    private void getNumberFromTenToTwenty(int number) {
        if (number > 10 && number < 20) {
            result = tenToTwentyNames[getSecondDigit(number)];
        }
    }

    private void getTwoDigitNumber(int number) {
        if (number >= 20 && number < 100 && String.valueOf(number).endsWith("0")) {
            result = tensNames[getFirstDigit(number)];
        } else if (number > 20 && number < 100) {
            result = concatString(getFirstDigit(number), getSecondDigit(number));
        }
    }

    private void getThreeDigitNumber(int number) {
        if (number >= 100 && number < 1000 && String.valueOf(number).endsWith("00")) {
            result = oneDigitNames[getFirstDigit(number)] + " " + HUNDRED;
        } else if (number > 100 && number < 1000) {
            if (getSecondDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + HUNDRED + " " + oneDigitNames[getThirdDigit(number)];
            } else if (getThirdDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + HUNDRED + " " + tensNames[getSecondDigit(number)];
            } else {
                result = oneDigitNames[getFirstDigit(number)] + " " + HUNDRED + " "
                        + concatString(getSecondDigit(number), getThirdDigit(number));
            }
        }
    }

    private void getFourDigitNumber(int number) {
        if (number >= 1000 && number < 10000 && String.valueOf(number).endsWith("000")) {
            result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND;
        } else if (number > 1000 && number < 10000) {
            if (getSecondDigit(number) == 0 && getFourthDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " " + tensNames[getThirdDigit(number)];
            } else if (getSecondDigit(number) == 0 && getThirdDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " " + oneDigitNames[getFourthDigit(number)];
            } else if (getSecondDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " "
                        + concatString(getThirdDigit(number), getFourthDigit(number));
            } else if (getThirdDigit(number) == 0 && getFourthDigit(number) == 0) {
                result = concatString(getFirstDigit(number), getSecondDigit(number)) + " " + HUNDRED;
            } else if (getThirdDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " "
                        + oneDigitNames[getSecondDigit(number)] + " " + HUNDRED + " "
                        + oneDigitNames[getFourthDigit(number)];
            } else if (getFourthDigit(number) == 0) {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " "
                        + oneDigitNames[getSecondDigit(number)] + " " + HUNDRED + " "
                        + tensNames[getThirdDigit(number)];
            } else {
                result = oneDigitNames[getFirstDigit(number)] + " " + THOUSAND + " "
                        + oneDigitNames[getSecondDigit(number)] + " " + HUNDRED + " "
                        + concatString(getThirdDigit(number), getFourthDigit(number));
            }
        }
    }

    private int getSubstring(int number, int begin, int end) {
        return Integer.parseInt(String.valueOf(number).substring(begin, end));
    }

    private int getFirstDigit(int number) {
        return getSubstring(number, 0, 1);
    }

    private int getSecondDigit(int number) {
        return getSubstring(number, 1, 2);
    }

    private int getThirdDigit(int number) {
        return getSubstring(number, 2, 3);
    }

    private int getFourthDigit(int number) {
        return getSubstring(number, 3, 4);
    }

    private String concatString(int first, int second) {
        return tensNames[first] + "-" + oneDigitNames[second];
    }
}
