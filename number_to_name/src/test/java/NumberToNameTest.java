import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class NumberToNameTest {

    private NumberToName numberToName;
    private final int number;
    private final String stringValue;

    public NumberToNameTest(int number, String stringValue) {
        this.number = number;
        this.stringValue = stringValue;
    }

    @Before
    public void setUp() {
        numberToName = new NumberToName();
    }

    @Parameters
    public static Collection<Object[]> getResults() {
        return Arrays.asList(new Object[][]{
                {0, "zero"},
                {1, "one"},
                {2, "two"},
                {3, "three"},
                {4, "four"},
                {5, "five"},
                {6, "six"},
                {7, "seven"},
                {8, "eight"},
                {9, "nine"},

                {11, "eleven"},
                {12, "twelve"},
                {13, "thirteen"},
                {14, "fourteen"},
                {15, "fifteen"},
                {16, "sixteen"},
                {17, "seventeen"},
                {18, "eighteen"},
                {19, "nineteen"},

                {20, "twenty"},
                {80, "eighty"},

                {21, "twenty-one"},
                {77, "seventy-seven"},
                {55, "fifty-five"},

                {100, "one hundred"},
                {200, "two hundred"},
                {300, "three hundred"},
                {400, "four hundred"},
                {500, "five hundred"},
                {600, "six hundred"},
                {700, "seven hundred"},
                {800, "eight hundred"},
                {900, "nine hundred"},

                {303, "three hundred three"},
                {555, "five hundred fifty-five"},
                {499, "four hundred ninety-nine"},
                {990, "nine hundred ninety"},

                {2000, "two thousand"},
                {3466, "three thousand four hundred sixty-six"},
                {2400, "twenty-four hundred"},
                {2070, "two thousand seventy"},
                {2077, "two thousand seventy-seven"},
                {2770, "two thousand seven hundred seventy"},
                {2007, "two thousand seven"},
        });
    }

    @Test
    public void convertNumberTest() {
        assertEquals(numberToName.convertNumber(number), stringValue);
    }
}
